import PropTypes from 'prop-types';
import ProductList from '../../ProductList/ProductList';

export default function HomepageBody({ children, products, onAddToCart, onToggleFavorite }) {

    HomepageBody.defaultProps = {
        children: null
    };

    if (!products.length) {
        return <div>Loading...</div>; // повідомлення про завантаження
    }

    return (
    <div className="homepage-body">
        <ProductList
        products={products}
        onAddToCart={onAddToCart}
        onToggleFavorite={onToggleFavorite}
        >
        </ProductList>
        {children}
    </div>
    );
}

HomepageBody.propTypes = {
    children: PropTypes.node,
    products: PropTypes.array.isRequired,
    onAddToCart: PropTypes.func,
    onToggleFavorite: PropTypes.func
}