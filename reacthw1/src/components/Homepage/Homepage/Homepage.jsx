import PropTypes from 'prop-types';
import HomepageHeader from '../HomepageHeader/HomepageHeader';
import HomepageBody from '../HomepageBody/HomepageBody';
import HomepageFooter from '../HomepageFooter/HomepageFooter';
import Modal from '../../Modal/Modal/Modal'
import ModalBody from '../../Modal/ModalBody/ModalBody';
import ModalClose from '../../Modal/ModalClose/ModalClose';
import ModalFooter from '../../Modal/ModalFooter/ModalFooter';
import ModalHeader from '../../Modal/ModalHeader/ModalHeader';
import ModalWrapper from '../../Modal/ModalWrapper/ModalWrapper';
import { useState, useEffect, useCallback } from 'react';

export default function Homepage ({children}) {
    
    // Homepage.defaultProps = {
    //     children: null
    // };

    const [firstModalVisible, setFirstModalVisible] = useState(false);
    const [thirdModalVisible, setThirdModalVisible] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [modalBodyText, setModalBodyText] = useState('');
    const [modalFirstButtonText, setModalFirstButtonText] = useState('');
    const [modalFirstButtonClick, setModalFirstButtonClick] = useState(() => {});

    const [selectedProductsCount, setSelectedProductsCount] = useState(0);
    const [cartItemsCount, setCartItemsCount] = useState(0);

    const [selectedProduct, setSelectedProduct] = useState(null);
    const [selectedProducts, setSelectedProducts] = useState([]);
    // let cartCount = 0;

    const toggleFirstModal = () => {
        setFirstModalVisible(prevState => !prevState);
    };
    const toggleThirdModal = () => {
        setThirdModalVisible(prevState => !prevState);
    };
    
    // Створення локального стану для збереження товарів
    const [products, setProducts] = useState([]);

    // Запит для отримання даних з сервера за допомогою useEffect
    useEffect(() => {
      // Виконання AJAX запиту
        fetch('../public/products.json')
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            // Оновлення стану з отриманими даними
            setProducts(data);
        })
        .catch(error => {
            console.error('There was a problem with your fetch operation:', error);
        });
    }, []);

    const updateCounts = useCallback((selectedProducts) => {
        const selectedCount = countSelected(selectedProducts, 'isFavorite');
        const newCartCount = countSelected(selectedProducts, 'isOnCart');
        setSelectedProductsCount(selectedCount);
        setCartItemsCount(newCartCount); // Оновлюємо лічильник товарів у корзині
    }, []);

    useEffect(() => {
        // Перевірка, чи є дані у локальному сховищі
        const savedProducts = JSON.parse(localStorage.getItem('selectedProducts'));
        if (savedProducts) {
            setSelectedProducts(savedProducts);
            updateCounts(savedProducts);
        } else {
            localStorage.setItem('selectedProducts', JSON.stringify([]));
            setSelectedProducts([]);
            setSelectedProductsCount(0);
            setCartItemsCount(0);
        }
    }, []);

    useEffect(() => {
        // Очищення даних при перезавантаженні сторінки
        window.addEventListener('beforeunload', () => {
            localStorage.clear();
        });
        return () => {
            window.removeEventListener('beforeunload', () => {
                localStorage.clear();
            });
        };
    }, []);

    useEffect(() => {
        console.log("Selected Products Count:", selectedProductsCount);
    }, [selectedProductsCount, selectedProducts]);

    const handleToggleFavorite = (product) => {
        const alreadyAdded = selectedProducts.find((p) => p.sku === product.sku);
        let updatedProducts;
        if (!alreadyAdded) {
            updatedProducts = [...selectedProducts, product];
            setSelectedProductsCount(selectedProductsCount + 1); // Збільшення лічильника на 1
        } else {
            updatedProducts = selectedProducts.filter((p) => p.sku !== product.sku);
            setSelectedProductsCount(selectedProductsCount - 1); // Зменшення лічильника на 1
        }
        localStorage.setItem('selectedProducts', JSON.stringify(updatedProducts));
        setSelectedProducts(updatedProducts);
    };

    const handleToggleCart = (product, isOnCart) => {
        setSelectedProduct(product);
        if (isOnCart) {
            setModalBodyText(`${product.name} added to your cart`);
            setModalFirstButtonText('Okay');
            setModalFirstButtonClick(() => {
                addToCart(product);
                setModalVisible(true);
            });
            setModalVisible(true);
        }
    };

    const addToCart = (product) => {
        const alreadyAdded = selectedProducts.find((p) => p.sku === product.sku);
        let updatedProducts;
        if (!alreadyAdded) {
            updatedProducts = [...selectedProducts, { ...product, isOnCart: true }];
        } else {
            updatedProducts = selectedProducts.map((p) =>
                p.sku === product.sku ? { ...p, isOnCart: true } : p
            );
        }
        localStorage.setItem('selectedProducts', JSON.stringify(updatedProducts));
        setSelectedProducts(updatedProducts);
        updateCounts(updatedProducts);
        toggleThirdModal();
        setCartItemsCount(updatedProducts.filter(p => p.isOnCart).length); // Оновлення лічильника
    };
    
    const countSelected = (selectedProducts, type) => {
        if (!selectedProducts) return 0;
        return selectedProducts.filter(product => product && product[type]).length;
    };
    
    
    const deleteProduct = (productSku) => {
        const updatedProducts = selectedProducts.filter(product => product.sku !== productSku);
        localStorage.setItem('selectedProducts', JSON.stringify(updatedProducts));
        setSelectedProducts(updatedProducts);
        setSelectedProductsCount(selectedProductsCount - 1); // Зменшення лічильника на 1
        setCartItemsCount(countSelected(updatedProducts, 'isOnCart'));
    };

    const closeModal = () => {
        setModalVisible(false);
    };

    return (
        <>
        <div className="homepage">
            <HomepageHeader
                selectedProductsCount={selectedProductsCount}
                cartItemsCount={cartItemsCount}
            >        
            </HomepageHeader>
            <HomepageBody
            products={products}
            onAddToCart={handleToggleCart}
            onToggleFavorite={handleToggleFavorite}
            >
            </HomepageBody>
            <HomepageFooter></HomepageFooter>
            {children}
        </div>        
        
        {firstModalVisible && (
            <Modal className={"firstModal"} onClose={toggleFirstModal} isVisible={firstModalVisible}>
                <ModalWrapper onClose={toggleFirstModal}>
                    <ModalClose onClick={toggleFirstModal}/>
                    <ModalHeader>
                        <h2>Product Delete!</h2>
                    </ModalHeader>
                    <ModalBody>
                        <p> By clicking the “Yes, Delete” button, Clancy will be deleted.</p>
                    </ModalBody>
                    <ModalFooter
                        firstText={"no, cancel"}
                        firstClick={toggleFirstModal}
                        secondaryText={"yes, delete"}
                        secondaryClick={deleteProduct}>
                    </ModalFooter>
                </ModalWrapper>          
            </Modal>
        )}

        {thirdModalVisible &&(
            <Modal
                className={"secondModal cart"}
                isVisible={modalVisible}
                onClose={closeModal}
                headerText={selectedProduct && selectedProduct.name}
                bodyText={modalBodyText}
                firstButtonText={modalFirstButtonText}
                firstButtonClick={modalFirstButtonClick}>
            </Modal>
        )}
        </>
    )
}
Homepage.propTypes = {
    children: PropTypes.node
}