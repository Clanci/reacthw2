import PropTypes from 'prop-types';

export default function HomepageFooter({ children }) {

    HomepageFooter.defaultProps = {
        children: null
    };

    return (
    <div className="homepage-footer">
        {children}
    </div>
    );
}

HomepageFooter.propTypes = {
    children: PropTypes.node
}