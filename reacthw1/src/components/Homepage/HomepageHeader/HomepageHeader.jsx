import PropTypes from 'prop-types';
import imageCart from '../../../assets/cart.png';
import imageStar from '../../../assets/star2.png';

export default function HomepageHeader({ children, selectedProductsCount, cartItemsCount }) {
    
    HomepageHeader.defaultProps = {
        children: null
    };

    return (
    <div className="homepage-header">
        <section>
            <p>{selectedProductsCount}</p>
            <img src={imageStar} alt="" />
        </section>
        <section>
            <p>{cartItemsCount}</p>
            <img src={imageCart} alt="" />
        </section>
        {children}
    </div>
    );
}

HomepageHeader.propTypes = {
    children: PropTypes.node,
    selectedProductsCount: PropTypes.number.isRequired,
    cartItemsCount: PropTypes.number.isRequired
}