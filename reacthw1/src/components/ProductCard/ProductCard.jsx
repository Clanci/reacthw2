import PropTypes from 'prop-types';
import { useState } from 'react';
import Button from '../Button/Button';
import star1 from '../../assets/star1.png';
import star2 from '../../assets/star2.png';

export default function ProductCard ({ product, onAddToCart, onToggleFavorite }) {
    const { name, price, image, color, isFavorite, isOnCart } = product;
    const [localIsFavorite, setLocalIsFavorite] = useState(isFavorite || false);
    const [localIsOnCart, setLocalIsOnCart] = useState(isOnCart || false)

    const handleToggleAddCart = () => {
        const newOnCartState = !localIsOnCart;
        setLocalIsOnCart(newOnCartState);
        onAddToCart(product, newOnCartState);
    };

    const handleToggleFavorite = () => {
        const newFavoriteState = !localIsFavorite;
        setLocalIsFavorite(newFavoriteState);
        onToggleFavorite(product, newFavoriteState);
    };

    return (
        <div className="product-card">
            <section className="product-card-title">
                <h3>{name}</h3>
            </section>
            <img src={image} alt={name} />
            <section className="product-card-caption">
                <p>Price: {price} $</p>
                <p>Color: {color}</p>
            </section>
            <section className="product-card-buttons">
                <Button type="button" classNames="product-card-cart_btn" onClick={handleToggleAddCart}>
                    {localIsOnCart ? "Added to cart" : "Add to cart"}
                </Button>
                <Button type="button" classNames="product-card-fav_btn" onClick={handleToggleFavorite}>
                    {localIsFavorite ?  <img src={star2} alt="#" /> :  <img src={star1} alt="#" />}
                </Button>
            </section>
        </div>
    );
}

ProductCard.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        image: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired,
        isFavorite: PropTypes.bool,
        isOnCart: PropTypes.bool
    }).isRequired,
    onAddToCart: PropTypes.func,
    onToggleFavorite: PropTypes.func
}