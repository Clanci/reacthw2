import PropTypes from 'prop-types';
import ProductCard from '../ProductCard/ProductCard';

export default function ProductList ({products, onAddToCart, onToggleFavorite}) {
    if (!products || products.length === 0) {
        return <div>No products available</div>;
    }// перевірка для того, щоб при завантаженні сторінки не виникало помилок
    
    return (
        <div className="product-list">
            {/* map замість forEach так як forEach нічого не повертає*/}
            {products.map((product) => (
                // щоб пройтися по кожному елементу у products та створити карточку товару за допомогою ProductCard
                <ProductCard
                    key={product.sku}
                    product={product}
                    onAddToCart={onAddToCart}
                    onToggleFavorite={onToggleFavorite}
                />
            ))}
        </div>
    );
}

ProductList.propTypes = {
    products: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired,
            image: PropTypes.string.isRequired,
            color: PropTypes.string.isRequired,
            isFavorite: PropTypes.bool,
        })
    ).isRequired,
    onAddToCart: PropTypes.func,
    onToggleFavorite: PropTypes.func
}