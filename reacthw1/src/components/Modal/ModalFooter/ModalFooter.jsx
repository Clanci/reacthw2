import PropTypes from 'prop-types';
import Button from '../../Button/Button';

export default function ModalFooter ({firstText, firstClick }) {
    const handleFirstClick = () => {
        if (firstClick) {
            firstClick();
        }
    };

    return (
        <div className="modal-footer">
            {firstText && (
                <Button onClick={handleFirstClick}>{firstText}</Button>
            )}
        </div>
    )
}

ModalFooter.propTypes = {
    firstText: PropTypes.string,
    firstClick: PropTypes.func,
}