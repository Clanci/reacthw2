import PropTypes from 'prop-types';

export default function ModalBody({ children }) {
    
    ModalBody.defaultProps = {
        children: null
    };

    return (
    <div className="modal-body">
        {children}
    </div>
    );
}

ModalBody.propTypes = {
    children: PropTypes.node
}