import PropTypes from 'prop-types';
import ModalWrapper from '../ModalWrapper/ModalWrapper';
import ModalHeader from '../ModalHeader/ModalHeader';
import ModalBody from '../ModalBody/ModalBody';
import ModalFooter from '../ModalFooter/ModalFooter';
import ModalClose from '../ModalClose/ModalClose';

export default function Modal ({children, classNames, isVisible, onClose, headerText, bodyText, firstButtonClick, selectedProduct}) {

    Modal.defaultProps = {
        children: null
    };

    const handleFirstClick = () => {
        onClose();
        if (firstButtonClick) {
            firstButtonClick(selectedProduct);
        }
    };

    return (
        <>
        {isVisible && (
            <ModalWrapper className={classNames} onClose={onClose}>
                <ModalClose onClick={onClose}/>
                <ModalHeader>
                    <h2>{headerText}</h2> 
                </ModalHeader>
                <ModalBody>
                    <p>{bodyText}</p>
                </ModalBody>
                <ModalFooter firstText={"Okay"} firstClick={handleFirstClick}>
                </ModalFooter>
            </ModalWrapper>
        )}
        {children}
        </>
        
    )
}

Modal.propTypes = {
    children: PropTypes.node,
    classNames: PropTypes.string,
    isVisible: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    headerText: PropTypes.string.isRequired,
    bodyText: PropTypes.string.isRequired,
    firstButtonClick: PropTypes.func,
    selectedProduct: PropTypes.object
}